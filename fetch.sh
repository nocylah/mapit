#!/bin/bash

mkdir -p data && cd data

wget https://download.geofabrik.de/north-america/us/new-mexico-latest.osm.pbf
spatialite_osm_raw new-mexico-latest.osm.pbf -d osm.sqlite3

poetry run eio --product SRTM3 clip -o srtm3.tif --bounds -109.353306 31.261342 -102.502758 37.230569
gdal_contour -i 30 -a height srtm3.tif contours.shp
spatialite_tool -i -d osm.sqlite3 -s 4326 -t contours -shp contours -c utf-8
