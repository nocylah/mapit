from shapely import geometry as Geom
import svgwrite as svg


def from_point(point):
    return svg.container.Use('', point.coords[0])


def from_line_string(line_string):
    return svg.shapes.Polyline(line_string.coords)


def from_linear_ring(linear_ring):
    return svg.shapes.Polygon(linear_ring.coords)


def from_polygon(polygon):
    group = svg.container.Group(fill_rule='evenodd')
    group.add(from_linear_ring(polygon.exterior))
    for hole in polygon.interiors:
        group.add(from_linear_ring(hole))
    return group


def from_multi_point(multi_point):
    for point in multi_point.geoms:
        yield from_point(point)


def from_multi_line_string(multi_line_string):
    for line in multi_line_string.geoms:
        yield from_line_string(line)


def from_multi_polygon(multi_polygon):
    for polygon in multi_polygon.geoms:
        yield from_polygon(polygon)


def from_geometry_collection(collection):
    for geom in collection.geoms:
        yield from from_shape(geom)


def from_shape(geom):
    if isinstance(geom, Geom.GeometryCollection):
        yield from from_geometry_collection(geom)
    if isinstance(geom, Geom.MultiPolygon):
        yield from from_multi_polygon(geom)
    elif isinstance(geom, Geom.MultiLineString):
        yield from from_multi_line_string(geom)
    elif isinstance(geom, Geom.MultiPoint):
        yield from from_multi_point(geom)
    elif isinstance(geom, Geom.Polygon):
        yield from_polygon(geom)
    elif isinstance(geom, Geom.LineString):
        yield from_line_string(geom)
    elif isinstance(geom, Geom.LinearRing):
        yield from_linear_ring(geom)
    elif isinstance(geom, Geom.Point):
        yield from_point(geom)
