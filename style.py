from collections import namedtuple

import svgwrite as svg


NORM = 2000  # Size of map, prior to transformation to match viewport size
DEFS = []  # Additional SVG des
RULES = []  # Ordered list of rules for selecting and styling OSM data
CONTOUR = None  # Optional contour-lines style
BACKGROUND = None  # Optional background fill style
DISC = True  # If True, clip map to a disc shape

# element node|way|relation
Rule = namedtuple('Rule', ['tags', 'element', 'shape_style',
                           'label_style', 'label_tag'],
                  defaults=('name', None, None))

#                               SVG Definitions
_stipple = svg.pattern.Pattern(id='stipple', x=0, y=0, width=20, height=20,
                               patternUnits='userSpaceOnUse')
_stipple.add(svg.shapes.Circle((10, 10), 5, fill='#FAFAFA'))
_stipple.rotate(15)
DEFS.append(_stipple)

_point = svg.container.Symbol(id='point', overflow="visible", debug=False)
_point.add(svg.shapes.Circle((0, 0), 4))
DEFS.append(_point)

_tri = svg.container.Symbol(id='tri', overflow="visible", debug=False)
_tri_d = 20
_tri_h = (3*_tri_d**2/4)**0.5
_tri.add(svg.shapes.Polygon(((-_tri_d/2, _tri_h/2), (_tri_d/2, _tri_h/2),
                             (0, -_tri_h/2))))
DEFS.append(_tri)

#                                     Etc
CONTOUR = dict(fill='none', stroke='#DFC27D', stroke_width=1.0)
BACKGROUND = dict(fill='url(#stipple)')

#                                    Styles
_style_walk = dict(fill='none', stroke='#8C510A', stroke_width=1.5,
                   stroke_dasharray=4)
_style_building = (
    dict(fill='#AAAAAA', stroke='#000000', stroke_width=0.5),
    dict(fill='#CCCCCC', stroke='#000000', stroke_width=0.5),
    dict(fill='#EEEEEE', stroke='#000000', stroke_width=0.5)
)
_style_waterway = dict(fill='none', stroke='#6BAED6', stroke_width=1)
_style_water = dict(fill='#6BAED6', stroke='black', stroke_width=0.5)
_style_spring = dict(href='#point', fill='#6BAED6', stroke='black',
                     stroke_width=1)
_style_peak = dict(href='#tri', fill='#D08F55')
_style_track = dict(fill='none', stroke='#CC2682', stroke_width=0.5)
_style_forest = dict(fill='#41AB5D')
_style_road_base = dict(fill='none', stroke='#AA0000')
_style_boundary = dict(fill='none', stroke='#969696', stroke_width=1)
_style_rail = dict(fill='none', stroke='#9EBCDA', stroke_width=1,
                   stroke_dasharray='4 1')
_style_amenity = dict(fill='#FFFFB3')
_style_industrial = dict(fill='#D4B9DA')
_style_farmland = dict(fill='#DFC27D')
_style_park = dict(fill='#A1D99B')
_style_military = dict(fill='#FED976')
_style_power = dict(fill='none', stroke='#F768A1', stroke_width=1,
                    stroke_dasharray=12)

_style_labels = dict(fill='black', stroke='white', stroke_width=5,
                     paint_order='stroke fill markers',
                     font_family='Noto Sans', font_size=15)

RULES = [
    # Rule({'boundary': None}, 'relation', _style_boundary),

    Rule({'landuse': 'forest'}, 'way', _style_forest, _style_labels),
    Rule({'landuse': 'agriculture'}, 'way', _style_farmland),
    Rule({'landuse': 'farmland'}, 'way', _style_farmland),
    Rule({'landuse': 'industrial'}, 'way', _style_industrial),

    Rule({'natural': 'spring'}, 'node', _style_spring, _style_labels),
    Rule({'natural': 'water', 'water': 'river'}, 'way', _style_water,
         _style_labels),
    Rule({'natural': 'lake'}, 'way',  _style_water, _style_labels),
    Rule({'natural': 'swamp'}, 'way',  _style_water, _style_labels),
    Rule({'natural': 'wetland'}, 'way',  _style_water, _style_labels),
    Rule({'natural': 'rapids'}, 'way',  _style_water, _style_labels),
    Rule({'natural': 'wood'}, 'way',  _style_forest, _style_labels),
    Rule({'natural': 'tree'}, 'way',  _style_forest, _style_labels),
    Rule({'natural': 'scrub'}, 'way',  _style_forest, _style_labels),
    Rule({'natural': 'valley'}, 'way',  CONTOUR, _style_labels),
    Rule({'natural': 'peak'}, 'node',  _style_peak, _style_labels),

    Rule({'leisure': None}, 'way',  _style_park, _style_labels),
    Rule({'amenity': None}, 'way', _style_amenity, _style_labels),

    Rule({'highway': 'path'}, 'way', _style_walk, _style_labels),
    Rule({'highway': 'footway'}, 'way', _style_walk, _style_labels),
    Rule({'highway': 'bridleway'}, 'way', _style_walk, _style_labels),
    Rule({'highway': 'steps'}, 'way', _style_walk, _style_labels),
    Rule({'highway': 'cycleway'}, 'way', _style_walk, _style_labels),
    Rule({'highway': 'track'}, 'way', _style_track, _style_labels),
    Rule({'highway': 'motorway'}, 'way',
         dict(**_style_road_base, stroke_width=6)),
    Rule({'highway': 'trunk'}, 'way', dict(**_style_road_base, stroke_width=4),
         None),
    Rule({'highway': 'primary'}, 'way',
         dict(**_style_road_base, stroke_width=4)),
    Rule({'highway': 'secondary'}, 'way',
         dict(**_style_road_base, stroke_width=3)),
    Rule({'highway': 'tertiary'}, 'way',
         dict(**_style_road_base, stroke_width=1.5)),
    Rule({'highway': 'unclassified'}, 'way',
         dict(**_style_road_base, stroke_width=1)),
    Rule({'highway': 'service'}, 'way',
         dict(**_style_road_base, stroke_width=1)),
    Rule({'highway': 'residential'}, 'way',
         dict(**_style_road_base, stroke_width=1)),

    Rule({'waterway': None}, 'way',  _style_waterway),
    Rule({'building': None}, 'way', _style_building),
    Rule({'railway': None}, 'way', _style_rail),
    Rule({'military': None}, 'way', _style_rail),
    Rule({'power': None}, 'way', _style_power)
]
