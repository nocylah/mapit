#!/usr/bin/env python
import argparse
from random import choice
from collections import namedtuple
import sqlite3
import os
import sys
from math import floor

from shapely import affinity
from shapely import wkt as Wkt
from shapely.errors import WKTReadingError
import shapely.geometry as Geom
from shapely.ops import substring, polylabel

import svgwrite as svg

from .svg import from_shape


def draw_layer(drawing, labels, layer, geom_style, label_style, aoi, transform):
    minx, miny, maxx, maxy = aoi.bounds

    for geom, attrs in layer:
        geom = geom.intersection(aoi)
        if not geom:
            continue

        geom = affinity.affine_transform(geom, transform)

        if geom_style:
            for path in from_shape(geom):
                path._parameter.debug = False
                if isinstance(geom_style, tuple):
                    path.update(choice(geom_style))
                else:
                    path.update(geom_style)
                drawing.add(path)

        if label_style and 'name' in attrs:
            if isinstance(geom, Geom.Point):
                text = svg.text.Text(attrs['name'],
                                     insert=(path['x'], path['y'] + 20),
                                     text_anchor='middle',
                                     debug=False,
                                     **label_style)
                labels.add(text)

            if isinstance(geom, Geom.LineString):
                parts = floor(geom.length / 400)
                for p in range(parts):
                    piece = substring(geom, p/parts, (p+1)/parts, normalized=True)
                    if piece.coords[0][0] <= piece.coords[-1][0]:
                        piece.coords = list(piece.coords)[::-1]

                    piece = piece.parallel_offset(30, 'right', 30)
                    if isinstance(piece, Geom.MultiLineString) or piece.length == 0:
                        continue

                    path = next(from_shape(piece))
                    path.update(dict(visibility='hidden'))

                    drawing.add(path)
                    text = svg.text.TextPath(path, attrs['name'],
                                             startOffset='5%',
                                             debug=False,
                                             **label_style)

                    container = labels.add(svg.text.Text(''))
                    container.add(text)

            if isinstance(geom, Geom.Polygon):
                coords = tuple(*polylabel(geom).coords)
                if not coords:
                    continue
                text = svg.text.Text(attrs['name'],
                                     insert=coords,
                                     text_anchor='middle',
                                     debug=False,
                                     **label_style)
                labels.add(text)


def _query_nodes(crs, rule, bbox):
    query = ('SELECT node_id, astext(geometry) FROM osm_nodes WHERE '
             f'intersects(geometry, GeomFromText("{Wkt.dumps(bbox)}")) AND ')

    query += ' AND '.join(
        f'node_id IN (SELECT node_id FROM osm_node_tags WHERE k="{key}" AND v="{val}")'
        for key, val in rule.tags.items())

    crs.execute(query)
    for node_id, geostr in crs.fetchall():
        if not geostr:
            continue
        try:
            geom = Wkt.loads(geostr)
        except WKTReadingError:
            continue

        attrs = {}
        if rule.label_style:
            query = f'SELECT k, v FROM osm_node_tags WHERE node_id={node_id}'
            crs.execute(query)
            for k, v in crs.fetchall():
                attrs[k] = v

        yield geom, attrs


def _query_ways(crs, rule, bbox):
    query = f'''SELECT way_id, AsText(MakeLine(geometry)) FROM
        osm_way_refs INNER JOIN osm_nodes USING (node_id) WHERE
        way_id in (SELECT DISTINCT(way_id) FROM
            osm_way_refs INNER JOIN osm_nodes USING (node_id) WHERE
            intersects(geometry, GeomFromText("{Wkt.dumps(bbox)}")) AND '''

    query += ' AND '.join(
        f'way_id IN (SELECT way_id FROM osm_way_tags WHERE k="{key}"' +
        (f' AND v="{val}")' if val is not None else ')')
        for key, val in rule.tags.items())

    query += ') GROUP BY way_id'

    crs.execute(query)
    for way_id, geostr in crs.fetchall():
        if not geostr:
            continue
        try:
            geom = Wkt.loads(geostr)
        except WKTReadingError:
            continue

        if geom.is_ring:
            geom = Geom.Polygon(geom.coords)

        attrs = {}
        if rule.label_style:
            query = f'SELECT k, v FROM osm_way_tags WHERE way_id={way_id}'
            crs.execute(query)
            for k, v in crs.fetchall():
                attrs[k] = v

        yield geom, attrs


def _query_relations(crs, rule, bbox):
    visible_nodes = f'''SELECT node_id FROM osm_nodes WHERE
        intersects(geometry, GeomFromText("{Wkt.dumps(bbox)}"))'''

    visible_ways = f'''SELECT DISTINCT(way_id) FROM osm_way_refs WHERE
        node_id in ({visible_nodes})'''

    matched_ways = f'''SELECT rel_id, ref as way_id FROM osm_relation_refs WHERE
        type='W' AND ref in ({visible_ways}) AND ''' + ' AND '.join(
        f'rel_id IN (SELECT rel_id FROM osm_relation_tags WHERE k="{key}"' +
        (f' AND v="{val}")' if val is not None else ')')
        for key, val in rule.tags.items())

    query = f'''SELECT rel_id, way_id, AsText(MakeLine(geometry)) FROM
        ({matched_ways}) INNER JOIN osm_way_refs USING(way_id)
            INNER JOIN osm_nodes USING (node_id) GROUP BY way_id'''

    crs.execute(query)
    for rel_id, way_id, geostr in crs.fetchall():
        if not geostr:
            continue
        try:
            geom = Wkt.loads(geostr)
        except WKTReadingError:
            continue

        if geom.is_ring:
            geom = Geom.Polygon(geom.coords)

        attrs = {}
        if rule.label_style:
            query = f'SELECT k, v FROM osm_relation_tags WHERE rel_id={rel_id}'
            crs.execute(query)
            for k, v in crs.fetchall():
                attrs[k] = v

        yield geom, attrs


def query_osm(crs, rule, bbox):
    print(f'{rule.element}: {rule.tags}')

    if rule.element.lower() == 'node':
        return _query_nodes(crs, rule, bbox)
    elif rule.element.lower() == 'way':
        return _query_ways(crs, rule, bbox)
    elif rule.element.lower() == 'relation':
        return _query_relations(crs, rule, bbox)
    else:
        return ()


def draw_map(drawing, crs, center, radius, norm, rules, contour=None,
             disc=True):
    aoi = center.buffer(radius)
    if not disc:
        aoi = aoi.envelope

    minx, miny, maxx, maxy = aoi.bounds
    scale = (norm/(maxx - minx), -norm/(maxy - miny))
    translate = -(minx * scale[0]), -((maxy) * scale[1])
    transform = [scale[0], 0, 0, scale[1], *translate]

    labels = svg.container.Group()

    postfix = f'WHERE intersects(geometry, GeomFromText("{Wkt.dumps(aoi)}")) '

    if contour is not None:
        crs.execute('SELECT astext(geometry), NULL from contours ' + postfix)
        contours = ((Wkt.loads(geostr), None) for geostr, _ in crs.fetchall())
        draw_layer(drawing, labels, contours, contour, None,
                   aoi, transform)

    for rule in rules:
        results = query_osm(crs, rule, aoi)
        draw_layer(drawing, labels, results,
                   rule.shape_style, rule.label_style,
                   aoi, transform)

    drawing.add(labels)


def get_sql(filename):
    conn = sqlite3.connect(filename)
    conn.enable_load_extension(True)
    conn.execute('SELECT load_extension("mod_spatialite")')
    return conn


def run():
    description = '''
    Create a map from OSM data. Try: mapit 35.0898 -106.6808 0.01
    '''
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('lat', type=float, help='latitude (wgs84)')
    parser.add_argument('lon', type=float, help='longitude (wgs84)')
    parser.add_argument('radius', type=float, help='radius in degrees (wgs84)')
    parser.add_argument('--edge_size', type=int, default=1024,
                        help='width/height of svg')
    parser.add_argument('--sql-path', default='data/osm.sqlite3',
                        help='spatialite database path')
    parser.add_argument('--style-path', default='.', help='path to style.py')
    parser.add_argument('--output', default='map.svg', help='output filepath')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()
    if 'help' in args:
        sys.exit(1)

    sys.path.append(os.path.abspath(args.style_path))
    from style import NORM, RULES, DEFS, CONTOUR, BACKGROUND, DISC

    center = Geom.Point(args.lon, args.lat)

    drawing = svg.Drawing(args.output, (args.edge_size, args.edge_size),
                          debug=False)
    group = svg.container.Group()
    group.scale(args.edge_size/NORM, args.edge_size/NORM)

    for pattern in DEFS:
        drawing.defs.add(pattern)

    if BACKGROUND is not None:
        group.add(svg.shapes.Rect((0, 0), ('200%', '200%'), **BACKGROUND))

    drawing.add(group)

    conn = get_sql(args.sql_path)
    draw_map(group, conn.cursor(), center, args.radius, NORM, RULES,
             contour=CONTOUR, disc=DISC)

    drawing.save()


if __name__ == '__main__':
    run()
